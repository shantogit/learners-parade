# **Run Docker in Windows 11 using WSL**  

### (Make sure WSL is enabled and Ubuntu & Docker is installed)

### Start WSL
```
wsl -d Ubuntu-22.04
```
### Mount E drive (Can be any other drive)
```
sudo mount -t drvfs 'E:' /mnt/e
```
### Check for docker image
```
docker images
```
You will see the list of docker images with image id. 

### Bind Mount docker container on a specific folder
```
docker container run --mount type=bind,sounce=<local directory>,target=/home -it --rm <image id> bash
```
**Example:** 
```
docker container run --mount type=bind,source=/mnt/e/Docker_Pipe/Transform,target=/home -v /mnt/e/Docker_Pipe/Common:/drive -it --rm transform bash
```
### Volume Mount docker container on a specific folder
```
docker container run --mount type=volume,sounce=<volume name>,target=/home -it --rm <image id> bash
```
**Example:** 
```
docker container run --mount type=bind,source=common,target=/home -v /mnt/e/Docker_Pipe/Common:/drive -it --rm transform bash
```

### Do both Volume & Bind Mount docker container on a specific folder
```
docker container run --mount type=bind,sounce=<local directory>,target=/home  --mount type=volume,sounce=<volume name>,target=/home-it --rm <image id> bash
```
**Example:** 
```
docker container run   --mount type=bind,source=/mnt/e/Docker_Pipe/Predict,target=/home   --mount type=volume,source=common,target=/drive   -it --rm predict bash -c "python3 /home/Predict.py"
```
### See Running & Closed Containers
```
docker ps -a
```
### Save Container as Image
```
docker commit <container_id> <new_image_name>
```
### Remove Container 
```
docker rm <container id>
```
### Remove All Container 
```
docker container prune
```
